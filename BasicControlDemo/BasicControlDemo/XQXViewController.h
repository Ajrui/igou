//
//  XQXViewController.h
//  BasicControlDemo
//
//  Created by fmning on 13-8-14.
//  Copyright (c) 2013年 fmning. All rights reserved.
//  viewcontroller

#import <UIKit/UIKit.h>

@interface XQXViewController : UIViewController
- (IBAction)clickSegmentButton:(id)sender;
- (IBAction)changeAnimate:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *anivityDicator;
@property (strong, nonatomic) IBOutlet UIProgressView *currentProgress;
- (IBAction)changeProgress:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *labelInfo;
- (IBAction)sliderChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lableSliderValue;

@end
