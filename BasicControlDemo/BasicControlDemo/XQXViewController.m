//
//  XQXViewController.m
//  BasicControlDemo
//
//  Created by fmning on 13-8-14.
//  Copyright (c) 2013年 fmning. All rights reserved.
//

#import "XQXViewController.h"

@interface XQXViewController ()

@end

@implementation XQXViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.imageView.image = [UIImage imageNamed:@"email.png"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSegmentButton:(id)sender {
    UISegmentedControl *button = (UISegmentedControl *)sender;
    int index = button.selectedSegmentIndex;
    switch (index) {
        case 0:
            self.labelInfo.text = @"first";
            break;
        case 1:
            self.labelInfo.text = @"second";
            break;
        case 2:
            self.labelInfo.text = @"third";
            break;
            
        default:
            break;
    }
}

//开关按钮事件
- (IBAction)changeAnimate:(id)sender {
    UISwitch *currentSwitch = (UISwitch *)sender;
    if (currentSwitch.isOn) {
        [self.anivityDicator startAnimating];
    }
    else{
        [self.anivityDicator stopAnimating];
    }
}

- (IBAction)sliderChanged:(id)sender {
    UISlider *slider = (UISlider *)sender;
    float v = slider.value;
    self.lableSliderValue.text = [NSString stringWithFormat:@"%.2f",v];
    
}
- (IBAction)changeProgress:(id)sender {
    
    UIButton *button =(UIButton *)sender;
    if (button.tag == 0) {
        self.currentProgress.progress += 0.1;
    }
    else{
        self.currentProgress.progress -= 0.1;
    }
    
}
@end


















