//
//  XQXAppDelegate.h
//  BasicControlDemo
//
//  Created by fmning on 13-8-14.
//  Copyright (c) 2013年 fmning. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XQXViewController;

@interface XQXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) XQXViewController *viewController;

@end
