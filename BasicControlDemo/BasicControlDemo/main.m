//
//  main.m
//  BasicControlDemo
//
//  Created by fmning on 13-8-14.
//  Copyright (c) 2013年 fmning. All rights reserved.
//test9999

#import <UIKit/UIKit.h>

#import "XQXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XQXAppDelegate class]));
    }
}
